#------------------------------------------------------------------------------
#                  GEOS-Chem Global Chemical Transport Model                  !
#------------------------------------------------------------------------------
#BOP
#
# !INCLUDE: UnitTest.specialty
#
# !DESCRIPTION: Input file that specifies debugging options for the 
#  GEOS-Chem unit tester. Customized for specialty (aka "offline") simulations.
#\\
#\\
# !REMARKS:  
#  To omit individual unit tests (or input settings), place a # comment
#  character in the first column.
#
#  For a complete explanation of how to customize the settings below for
#  your installation, please see these wiki posts:
#
#    http://wiki.geos-chem.org/GEOS-Chem_Unit_Tester#The_INPUTS_section
#    http://wiki.geos-chem.org/GEOS-Chem_Unit_Tester#The_RUNS_section
#
# !REVISION HISTORY: 
# Type 'gitk' at the prompt to browse the revision history.
#EOP
#------------------------------------------------------------------------------
#
# !INPUTS:
#
# %%% ID tags %%%
#
   VERSION          : v11-02
   DESCRIPTION      : Tests GEOS-Chem v11-02
#
# %%% Data path and HEMCO settings %%%
#
   DATA_ROOT        : /n/holylfs/EXTERNAL_REPOS/GEOS-CHEM/gcgrid/data/ExtData
   HEMCO_ROOT       : {DATAROOT}/HEMCO
   VERBOSE          : 3
   WARNINGS         : 3
#
# %%% Code and queue settings %%%
#
   CODE_DIR         : {HOME}/GC/Code.v11-02
   MAKE_CMD         : make -j4 BOUNDS=y DEBUG=y FPEX=y NO_ISO=y
   SUBMIT           : sbatch
#
# %%% Unit tester path names %%%
#
   UNIT_TEST_ROOT   : {HOME}/UT
   RUN_ROOT         : {UTROOT}/runs
   RUN_DIR          : {RUNROOT}/{RUNDIR}
   JOB_DIR          : {UTROOT}/jobs
   LOG_DIR          : {UTROOT}/logs/{VERSION}
   PERL_DIR         : {UTROOT}/perl
#
# %%% Web and text display options %%%
#
   TEMPLATE         : {PERLDIR}/ut_template.html
   TXT_GRID         : {LOGDIR}/{VERSION}.results.txt
   WEB_GRID         : {LOGDIR}/{VERSION}.results.html
   WEB_PUSH         : NONE
#
#  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
#  %%%  OPTIONAL SLURM COMMANDS:                                    %%%
#  %%%                                                              %%%
#  %%%  Set these if your system uses the SLURM scheduler.          %%%
#  %%%  These will be used to set #SBATCH tags for the job script.  %%%
#  %%%  Otherwise you can comment these lines out.                  %%%
#  %%%                                                              %%%
#  %%%  NOTE: If you do use these SLURM commands, then also be      %%%
#  %%%  sure to set the SUBMIT command above to "sbatch".           %%%
#  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
#
   SLURM_CPUS       : 4
   SLURM_NODES      : 1
   SLURM_TIME       : 1-00:00
   SLURM_MEMORY     : 12000
   SLURM_PARTITION  : jacob
   SLURM_CONSTRAINT : intel
   SLURM_MAILTYPE   : END
#
# !RUNS:
#  Specify the debugging runs that you want to perform below.
#  You can deactivate runs by commenting them out with "#".
#
#--------|-----------|------|------------|------------|--------------|--------|
# MET    | GRID      | NEST | SIMULATION | START DATE | END DATE     | EXTRA? |
#--------|-----------|------|------------|------------|--------------|--------|
# ======= Radon-Lead-Beryllium ===============================================
  geosfp   4x5         -      RnPbBe       2013070100   201307010020   -
  merra2   4x5         -      RnPbBe       2013070100   201307010020   -
  geosfp   2x25        -      RnPbBe       2013070100   201307010020   -
  merra2   2x25        -      RnPbBe       2013070100   201307010020   -
# ======= Mercury ============================================================
  geosfp   4x5         -      Hg           2013010100   201301010020   -
  merra2   4x5         -      Hg           2013010100   201301010020   -
  geosfp   2x25        -      Hg           2013010100   201301010020   -
  merra2   2x25        -      Hg           2013010100   201301010020   -
# ======= Tagged Mercury =====================================================
  geosfp   4x5         -      tagHg        2013010100   201301010020     -
  merra2   4x5         -      tagHg        2013010100   201301010020     -
# ======= POPs ===============================================================
  geosfp   4x5         -      POPs         2013070100   201307010020   -
  merra2   4x5         -      POPs         2013070100   201307010020   -
  geosfp   2x25        -      POPs         2013070100   201307010020   -
  merra2   2x25        -      POPs         2013070100   201307010020   -
# ======= Methane ============================================================
  geosfp   4x5         -      CH4          2013070100   201307010020   -
  merra2   4x5         -      CH4          2013070100   201307010020   -
  geosfp   2x25        -      CH4          2013070100   201307010020   -
  merra2   2x25        -      CH4          2013070100   201307010020   -
  geosfp   025x03125   na     CH4          2013070100   201307010010   -
  merra2   05x0625     na     CH4          2013070100   201307010020   -
# ======= Tagged O3 ==========================================================
  geosfp   4x5         -      tagO3        2013070100   201307010020   -
  merra2   4x5         -      tagO3        2013070100   201307010020   -
  geosfp   2x25        -      tagO3        2013070100   201307010020   -
  merra2   2x25        -      tagO3        2013070100   201307010020   -
# ======= Tagged CO ==========================================================
  geosfp   4x5         -      tagCO        2013070100   201307010020   -
  merra2   4x5         -      tagCO        2013070100   201307010020   -
  geosfp   2x25        -      tagCO        2013070100   201307010020   -
  merra2   2x25        -      tagCO        2013070100   201307010020   -
# ======= Carbon Dioxide =====================================================
  geosfp   2x25        -      CO2          2013070100   201307010020   -
  merra2   2x25        -      CO2          2013070100   201307010020   -
# ======= Offline Aerosols ===================================================
  geosfp   4x5         -      aerosol      2013070100   201307010020   - 
  merra2   4x5         -      aerosol      2013070100   201307010020   - 
  geosfp   2x25        -      aerosol      2013070100   201307010020   -
  merra2   2x25        -      aerosol      2013070100   201307010020   -
 !END OF RUNS:
#EOP
#------------------------------------------------------------------------------
